﻿/* Filename: Player.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist016 {
	public class Player : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "Player.cs: ";     

        [SerializeField]
        private GameObject goLattice;

        [SerializeField]
        private float factorRate = 0.25f;

        private List<Node> lattice = new List<Node>();
        private Node nodeCurrent;

        void Start()
        {
            foreach (Node n in goLattice.GetComponent<Lattice>().lattice) {
                lattice.Add(n);
            }

            nodeCurrent = lattice[0];
            this.transform.position = lattice[0].position;

            print(SCRIPTNAME + lattice.Count);

            //StartCoroutine(TraversePath(Lattice.CalculateShortestPath(
            //    lattice[0],
            //    lattice[lattice.Count - 1])));
        }

        void Update()
        {
            // Handles input
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {
                Move(Lattice.Direction.North);
            }
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) {
                Move(Lattice.Direction.West);
            }
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) {
                Move(Lattice.Direction.South);
            }
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) {
                Move(Lattice.Direction.East);
            }                 
        
        }

        public IEnumerator TraversePath(List<Node> path, float factorRate = 0.25f)
        {
            foreach (Node n in path) {
                MoveToNode(n);
                yield return new WaitForSeconds(factorRate);
            }
        }

        public void MoveToNode(Node n)
        {
            this.transform.position = n.position;
            nodeCurrent = n;
        }

        public void Move(Lattice.Direction dir)
        {
            switch (dir) {
                case Lattice.Direction.North:                    
                    this.transform.position = nodeCurrent.neighbours.North.position;
                    nodeCurrent = nodeCurrent.neighbours.North;
                    break;
                case Lattice.Direction.NorthEast:
                    this.transform.position = nodeCurrent.neighbours.NorthEast.position;
                    nodeCurrent = nodeCurrent.neighbours.NorthEast;
                    break;
                case Lattice.Direction.East:
                    this.transform.position = nodeCurrent.neighbours.East.position;
                    nodeCurrent = nodeCurrent.neighbours.East;
                    break;
                case Lattice.Direction.SouthEast:
                    this.transform.position = nodeCurrent.neighbours.SouthEast.position;
                    nodeCurrent = nodeCurrent.neighbours.SouthEast;
                    break;
                case Lattice.Direction.South:
                    this.transform.position = nodeCurrent.neighbours.South.position;
                    nodeCurrent = nodeCurrent.neighbours.South;
                    break;
                case Lattice.Direction.SouthWest:
                    this.transform.position = nodeCurrent.neighbours.SouthWest.position;
                    nodeCurrent = nodeCurrent.neighbours.SouthWest;
                    break;
                case Lattice.Direction.West:
                    this.transform.position = nodeCurrent.neighbours.West.position;
                    nodeCurrent = nodeCurrent.neighbours.West;
                    break;
                case Lattice.Direction.NorthWest:
                    this.transform.position = nodeCurrent.neighbours.NorthWest.position;
                    nodeCurrent = nodeCurrent.neighbours.NorthWest;
                    break;
            }
        }
	}
}
