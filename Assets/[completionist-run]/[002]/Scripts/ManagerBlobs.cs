﻿/* Filename: ManagerBlobs.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist002 {
	public class ManagerBlobs : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "ManagerBlobs.cs: ";

        public Material blobLitFull;
        public Material blobLitPartial;
        public Material blobUnlit;

        [SerializeField]
        private GameObject prefabBlob;

        [SerializeField]
        private GameObject screenVictory;

        [SerializeField]
        int countRow, countColumn = 10;

        private List<EntityBlob> blobs = new List<EntityBlob>();

        void Start()
        {
            // Deactivates victory screen
            screenVictory.SetActive(false);

            // Populates blobs
            foreach (EntityBlob b in GameObject.FindObjectsOfType<EntityBlob>()) {
                blobs.Add(b);
                print(SCRIPTNAME + "blob added from " + b.name);
            }

            // Generates blobs
            GenerateBlobs(countRow, countColumn);
        }

        void LateUpdate()
        {
            CheckVictory();
        }

        private void GenerateBlobs(int countRow, int countColumn)
        {
            for (int row = 0; row < countRow; row++) {
                for (int col = 0; col < countColumn; col++) {
                    if (UnityEngine.Random.Range(0f, 1f) <= 0.65f) {
                        GameObject go;
                        go = (GameObject)Instantiate(prefabBlob, 2f * new Vector3(row, 0f, col), Quaternion.identity);
                        go.transform.parent = this.transform;
                    }
                }
            }
        }

        private bool CheckVictory()
        {
            foreach (EntityBlob b in blobs) {
                if (EntityBlob.States.Unlit == b.state) {
                    return false;
                } else {
                    StartCoroutine(DoVictory());
                    return true;
                }
            }

            return true;
        }

        IEnumerator DoVictory()
        {
            print(SCRIPTNAME + "You won!");

            GameObject.FindObjectOfType<EntityPlayer>().gameObject.SetActive(false);

            foreach (EntityBlob b in blobs)
                b.gameObject.SetActive(false);

            screenVictory.SetActive(true);

            yield return null;
        }
    }
}
