﻿/* Filename: EntityBlob.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist002 {
	public class EntityBlob : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "EntityBlob.cs: ";

        public enum States
        {
            Unlit,
            LitPartial,
            LitFull
        };

        public States state;

        private ManagerBlobs manager;
        private Renderer rnd;

        void Awake()
        {
            // Finds ManagerBlobs
            manager = GameObject.FindObjectOfType<ManagerBlobs>();

            // Assigns renderer
            rnd = this.GetComponent<Renderer>();
        }

        // Calls Light Coroutine
        public void LightUp()
        {
                StopAllCoroutines();
                StartCoroutine(Light(12f));
            
        }

        private IEnumerator Light(float duration)
        {
            // Change to LitFull state
            state = States.LitFull;
            rnd.material = manager.blobLitFull;
            this.transform.localScale = 2f * Vector3.one;
            yield return new WaitForSeconds(duration * 0.4f);

            // Change to LitPartial state
            state = States.LitPartial;
            rnd.material = manager.blobLitPartial;
            this.transform.localScale = 1.5f * Vector3.one;
            yield return new WaitForSeconds(duration * 0.6f);

            // Change to Unlit state
            state = States.LitFull;
            this.transform.localScale = 1f * Vector3.one;
            rnd.material = manager.blobUnlit;

            yield return null;
        }
		
		
	}
}
