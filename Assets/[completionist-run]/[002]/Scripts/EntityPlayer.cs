﻿/* Filename: EntityPlayer.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist002
{
	public class EntityPlayer : MonoBehaviour
    {
	
		private const string SCRIPTNAME = "EntityPlayer.cs: ";

        [SerializeField]
        private float radius = 1f;

        private enum Directions
        {
            North,
            East,
            West,
            South
        };
		
        void FixedUpdate()
        {
            // Handles input
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {
                MoveTo(Directions.North);
            }
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) {
                MoveTo(Directions.West);
            }
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) {
                MoveTo(Directions.South);
            }
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) {
                MoveTo(Directions.East);
            }
            

            // Casts light
            CastLight();

        }

        private Vector3 GetDirection(Directions dir)
        {
            if (Directions.North == dir) {
                return new Vector3(0, 0, 1);
            } else if (Directions.East == dir) {
                return new Vector3(1, 0, 0);
            } else if (Directions.West == dir) {
                return new Vector3(-1, 0, 0);
            } else if (Directions.South == dir) {
                return new Vector3(0, 0, -1);
            } else {
                return Vector2.zero;
            }
                    
        }

        // Lights up all blobs near player in the von Neumann sense
        private void CastLight()
        {
            foreach (Collider col in Physics.OverlapSphere(this.transform.position, radius)) {
                if (null != col && null == col.GetComponent<EntityPlayer>())
                    col.transform.gameObject.GetComponent<EntityBlob>().LightUp();
                
                // print(SCRIPTNAME + "casting light on " + col.transform.gameObject.name + "...");
            }

        }

        private void MoveTo(Directions dir)
        {
            RaycastHit hitInfo = new RaycastHit();
            bool isBlob = Physics.Raycast(this.transform.position, GetDirection(dir), out hitInfo, 2f * radius);

            // Switches player and blob
            if (isBlob) {
                Vector3 tmp = this.transform.position;
                this.transform.position = hitInfo.transform.position;
                hitInfo.transform.position = tmp;

                
            }

            print(SCRIPTNAME + "I tried moving!");

        }
		
	}
}
