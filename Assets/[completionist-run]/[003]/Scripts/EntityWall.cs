﻿// File: EntityWall.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist003
{
	public class EntityWall : MonoBehaviour
	{

		private const string SCRIPTNAME = "EntityWall.cs: ";

        public enum StateWall
        {
            Inactive,
            Active
        }

        [SerializeField]
        private StateWall state;

        [SerializeField]
        List<EntityPivot.StatePivot> statesInactive = new List<EntityPivot.StatePivot>();

        [SerializeField]
        private Material materialActive;

        [SerializeField]
        private Material materialInactive;

        private EntityPivot pivot;
        private Renderer rnd;
        private Collider col;

        void Update()
        {
            // Gets material
            rnd = this.GetComponent<Renderer>();

            // Gets collider
            col = this.GetComponent<Collider>();

            // Grabs pivot
            pivot = GameObject.FindObjectOfType<EntityPivot>();

            // Updates state
            if (statesInactive.Contains(pivot.state)) {
                state = StateWall.Inactive;
                rnd.material = materialInactive;
                col.enabled = false;
            } else if (StateWall.Inactive == state) {
                state = StateWall.Active;
                rnd.material = materialActive;
                col.enabled = true;
            }
        }
    }
}