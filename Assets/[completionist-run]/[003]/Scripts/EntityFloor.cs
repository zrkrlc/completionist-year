﻿// File: EntityFloor.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist003
{
	public class EntityFloor : MonoBehaviour
	{

		private const string SCRIPTNAME = "EntityFloor.cs: ";

		public enum StateFloor
        {
            Inactive,
            Active
        }

        [SerializeField]
        private StateFloor state;

        public enum TypeFloor
        {
            Victory,
            Teleporter,
            Hole,
        }

        [SerializeField]
        private TypeFloor type;

        [SerializeField]
        private Material materialVictory;

        [SerializeField]
        private Material materialTeleporter;

        [SerializeField]
        private Material materialHole;

        [SerializeField]
        private Transform target;

        void Start()
        {
            // Activates floor if state is set to Active at start
            if (StateFloor.Active == state)
                Activate();
        }

        void OnCollisionEnter(Collision col)
        {
            if (null != col.gameObject.GetComponent<EntityPlayer>()) {
                Activate(col.transform);
            }
        }

        private void Activate(Transform teleportee = null)
        {
            state = StateFloor.Active;

            StopAllCoroutines();
            if (TypeFloor.Victory == type) {
                StartCoroutine(DoVictory());
            }

            if (TypeFloor.Teleporter == type) {
                StartCoroutine(DoTeleporter(teleportee.transform));
            }

            if (TypeFloor.Hole == type) {
                StartCoroutine(DoHole());
            }
        }

        private IEnumerator DoVictory()
        {
            state = StateFloor.Active;
            this.GetComponent<Renderer>().material = materialVictory;

            print(SCRIPTNAME + "You won!");
            yield return null;
        }

        private IEnumerator DoTeleporter(Transform teleportee)
        {
            state = StateFloor.Active;
            this.GetComponent<Renderer>().material = materialTeleporter;

            teleportee.transform.position = target.position + 2f * Vector3.up;
            print(SCRIPTNAME + "Player was teleported to " + target.name + ".");
            yield return null;
        }

        private IEnumerator DoHole()
        {
            state = StateFloor.Active;
            this.GetComponent<Renderer>().material = materialHole;

            this.GetComponent<Collider>().enabled = false;
            yield return null;
        }
    }
}