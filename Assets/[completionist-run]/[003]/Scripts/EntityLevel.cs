﻿// File: EntityLevel.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist003
{
	public class EntityLevel : MonoBehaviour
	{

		private const string SCRIPTNAME = "EntityLevel.cs: ";

        public uint level = 0;

        private ManagerLevels manager;

        void Start()
        {
            manager = GameObject.FindObjectOfType<ManagerLevels>();
        }

        void OnTriggerEnter()
        {
            print(SCRIPTNAME + this.name + " WTF");
            manager.ChangeLevelTo(level);
        }
	}
}