﻿// File: EntityPivot.cs
// Description: 
// How to use:
// 	0) 

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist003
{
	public class EntityPivot : MonoBehaviour
	{

		private const string SCRIPTNAME = "EntityPivot.cs: ";

        public enum StatePivot
        {
            North,
            East,
            South,
            West
        };

        private enum Rotation
        {
            CW,
            CCW
        };

        [SerializeField]
        public StatePivot state = StatePivot.North;

        [SerializeField]
        private Transform player;

        private bool isRotating = false;
        

		void Update()
        {
            // Syncs pivot position with player
            this.transform.position = player.position + 10f * Vector3.up;

            // Handles input

            if (Input.GetKeyDown(KeyCode.Q) && !isRotating) {
                
                StartCoroutine(Rotate(Rotation.CCW));
            }
            if (Input.GetKeyDown(KeyCode.E) && !isRotating) {
                StartCoroutine(Rotate(Rotation.CW));
            }
        }

        // Rotates pivot
        // NOTE: the approach here is cryptic. You might as well write it in assembly.
        private IEnumerator Rotate(Rotation rot)
        {
            isRotating = true;

            // Obtains direction to rotate towards
            Vector3 directionOrthogonal = Vector3.Cross((Rotation.CW == rot ? 1 : -1) * this.transform.forward, this.transform.up);
            Vector3.Normalize(directionOrthogonal);
            
            // Updates state
            state = (StatePivot) mod(((int) state + (Rotation.CW == rot ? -1 : 1)), Enum.GetNames(typeof(StatePivot)).Length);

            // Rotates pivot toward directionOrthogonal 
            // NOTE: timeSinceRotationStart and deltaTime prevents rotation lock up
            float timeSinceRotationStart = Time.time;
            while (isRotating) {
                float deltaTime = Time.time - timeSinceRotationStart;
                this.transform.rotation = Quaternion.Slerp(
                    this.transform.rotation,
                    Quaternion.LookRotation(directionOrthogonal, this.transform.up),
                    5f * Time.fixedDeltaTime);

                if (1 - Vector3.Dot(this.transform.forward, directionOrthogonal) < 0.0005f || deltaTime >= 2f) {
                    this.transform.rotation = Quaternion.LookRotation(directionOrthogonal, this.transform.up);
                    isRotating = false;
                }

                yield return null;
            }
            
            yield return null;
        }

        // Takes the modulo of n wrt m; needed because % sucks
        int mod(int x, int m)
        {
            return (x % m + m) % m;
        }

    }
}