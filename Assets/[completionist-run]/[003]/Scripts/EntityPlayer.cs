﻿// File: EntityPlayer.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist003 {
	public class EntityPlayer : MonoBehaviour
	{

		private const string SCRIPTNAME = "EntityPlayer.cs: ";

        private enum Direction
        {
            Forward,
            Leftward,
            Backward,
            Rightward
        };

        private Rigidbody rb;

        [SerializeField]
        private Transform pivot;

        [SerializeField]
        private float factorSpeed = 1.4f;

        [SerializeField]
        private float factorFriction = 2.4f;

		void Start () {
            // Grabs rigidbody
            if (null == this.GetComponent<Rigidbody>())
                this.gameObject.AddComponent<Rigidbody>();
            else
                rb = this.GetComponent<Rigidbody>();
		}
		
		void FixedUpdate () {
            // Handles input
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
                Move(Direction.Forward);
            }
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
                Move(Direction.Leftward);
            }
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) {
                Move(Direction.Backward);
            }
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
                Move(Direction.Rightward);
            }

            // Simulates friction on player
            rb.velocity = Vector3.Lerp(rb.velocity, Vector3.zero, factorFriction * Time.fixedDeltaTime);

        }

        private void Move(Direction dir)
        {
            rb.velocity = Vector3.zero;

            if (Direction.Forward == dir) {
                rb.velocity += rb.velocity + factorSpeed * pivot.transform.forward;
            }
            if (Direction.Leftward == dir) {
                rb.velocity = Vector3.Project(rb.velocity, pivot.transform.forward);
                rb.velocity += rb.velocity - factorSpeed * pivot.transform.right;
            }
            if (Direction.Backward == dir) {
                rb.velocity = Vector3.Project(rb.velocity, pivot.transform.right);
                rb.velocity += rb.velocity - factorSpeed * pivot.transform.forward;
            }
            if (Direction.Rightward == dir) {
                rb.velocity = Vector3.Project(rb.velocity, pivot.transform.forward);
                rb.velocity += rb.velocity + factorSpeed * pivot.transform.right;
            }
        }
	}
}