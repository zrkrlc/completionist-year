﻿// File: ManagerLevels.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist003
{
	public class ManagerLevels : MonoBehaviour
	{

		private const string SCRIPTNAME = "ManagerLevels.cs: ";

        public uint levelCurrent = 0;

        [SerializeField]
        private List<GameObject> levels = new List<GameObject>();

        void Start()
        {
            ChangeLevelTo(levelCurrent);
        }

        public void ChangeLevelTo(uint level)
        {
            print(SCRIPTNAME + "changing level to " + level + "...");
            levelCurrent = level;

            for (int i = 0; i < levels.Count; i++) {
                levels[i].SetActive(i >= level);
            }
        }
	}
}