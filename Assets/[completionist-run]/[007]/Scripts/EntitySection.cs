﻿/* Filename: EntitySection.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist007 {
	public class EntitySection : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "EntitySection.cs: ";
     
        [SerializeField]
        private bool hasBeenActivated = false;

        private List<GameObject> prefabsSection = new List<GameObject>();
        private GameObject prefabSection;
        private ManagerSections manager;

        void Start()
        {
            this.GetComponent<Collider>().isTrigger = true;

            manager = GameObject.FindObjectOfType<ManagerSections>();
            prefabSection = manager.sections[UnityEngine.Random.Range(0, manager.sections.Count - 1)];
        }

		void OnTriggerEnter(Collider hit)
        {
            // Ensures prefabSection existence
            while (null == prefabSection)
                prefabSection = manager.sections[UnityEngine.Random.Range(0, manager.sections.Count - 1)];

            // Generates new sections
            if (null != hit.GetComponent<EntityPlayer>() && !hasBeenActivated) {
                hasBeenActivated = true;

                Vector3 velocityHit = hit.GetComponent<Rigidbody>().velocity;
                float multiplier = velocityHit.x >= 0 ? 1 : -1;

                GameObject go = (GameObject)GameObject.Instantiate(
                    prefabSection,
                    this.transform.position + new Vector3(multiplier * 16f, 0, 0),
                    Quaternion.identity);
                go.transform.parent = manager.transform;

            }
        }
	}
}
