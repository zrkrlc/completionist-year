﻿/* Filename: EntityPlatform.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist007 {
	public class EntityPlatform : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "EntityPlatform.cs: ";

        [SerializeField]
        private Material matActive;

        private bool hasBeenActivated = false;
		
        void OnCollisionEnter(Collision col)
        {
            if (null != col.gameObject.GetComponent<EntityPlayer>()) {
                print(SCRIPTNAME + "platform activated.");
                hasBeenActivated = true;
                this.GetComponent<Renderer>().material = matActive;
            }
        }
		
	}
}
