﻿/* Filename: EntityCamera.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist007
{
	public class EntityCamera : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "EntityCamera.cs: ";

        [SerializeField]
        private Vector3 offset;

        private EntityPlayer player;

        void Start()
        {
            player = GameObject.FindObjectOfType<EntityPlayer>();
        }

        void Update()
        {
            Vector3 toPlayer = player.transform.position - this.transform.position;
            this.transform.rotation = Quaternion.Lerp(
                this.transform.rotation, 
                Quaternion.LookRotation(toPlayer, Vector3.up), 
                Time.fixedDeltaTime);
            this.transform.position = Vector3.Lerp(
                this.transform.position,
                player.transform.position + offset,
                4f * Time.smoothDeltaTime);
        }
		
	}
}
