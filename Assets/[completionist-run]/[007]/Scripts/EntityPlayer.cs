﻿/* Filename: EntityPlayer.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist007 {
    [RequireComponent(typeof(Rigidbody))]
	public class EntityPlayer : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "EntityPlayer.cs: ";

        [SerializeField]
        Vector3 forward;

        [SerializeField]
        float factorSpeed = 3.0f;

        [SerializeField]
        float factorJumpHeight = 4.0f;

        private Rigidbody rb;
        private bool isGrounded = false;

        void Start()
        {
            rb = this.GetComponent<Rigidbody>();
            
        }

        void FixedUpdate()
        {
            if (true == Physics.Raycast(this.transform.position, Vector3.down, 0.5f)) {
                // print(SCRIPTNAME + "Player is grounded.");
                
                isGrounded = true;
            } else {
                isGrounded = false;
            }
        }

        void Update()
        {
            // Handles input
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
                rb.velocity = factorSpeed * Vector3.Cross(Vector3.up, forward) + new Vector3(0, rb.velocity.y, 0);
            }
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
                rb.velocity = factorSpeed * -Vector3.Cross(Vector3.up, forward) + new Vector3(0, rb.velocity.y, 0);                
            }
            if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow) ||
                Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow)) {
                rb.velocity = new Vector3(0, rb.velocity.y, 0);
            }
            if (Input.GetKeyDown(KeyCode.Space) && isGrounded) {
                rb.velocity = new Vector3(rb.velocity.x, factorJumpHeight, rb.velocity.z);
            }
            
        }
	}
}
