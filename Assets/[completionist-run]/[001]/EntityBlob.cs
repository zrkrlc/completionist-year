﻿/* Filename: EntityBlob.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist001
{
	public class EntityBlob : MonoBehaviour {
	
		private const string SCRIPTNAME = "EntityBlob.cs: ";
		
		public enum State
        {
            Red,
            Green,
            Blue
        };

        public State state;

        private int countStates;

        [SerializeField]
        private ManagerBlobs managerBlobs;


        void Start()
        {
            countStates = Enum.GetNames(typeof(State)).Length;

            // Initialise blob
            state = (State) UnityEngine.Random.Range(0, countStates - 1);
            this.GetComponent<Renderer>().material.color = GetColor(state);

            // Sets manager
            managerBlobs = this.transform.parent.GetComponent<ManagerBlobs>();
        }

        void OnMouseUp()
        {
            managerBlobs.UpdateBlobsExcept(this.GetComponent<EntityBlob>());
        }

        public void Next(bool isActivated)
        {
            if (isActivated) {
                // Update blob state
                state = (State)(((int)state + 1) % countStates);
                this.GetComponent<Renderer>().material.color = GetColor(state);

                // Check for victory
                managerBlobs.CheckVictory(true);
            }
        }

        Color GetColor(State _state)
        {
            if (State.Red == _state)
                return Color.red;
            else if (State.Green == _state)
                return Color.green;
            else
                return Color.blue;
        }     

        
	}
}
