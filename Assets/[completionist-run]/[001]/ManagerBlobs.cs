﻿/* Filename: ManagerBlobs.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist001
{
	public class ManagerBlobs : MonoBehaviour {
	
		private const string SCRIPTNAME = "ManagerBlobs.cs: ";

        public List<EntityBlob> blobs = new List<EntityBlob>();

        [SerializeField]
        GameObject textVictory;

        void Start()
        {
            foreach(EntityBlob b in GameObject.FindObjectsOfType<EntityBlob>()) {
                blobs.Add(b);
                print(SCRIPTNAME + "blob added from " + b.name);
            }

            // Disable victory text
            textVictory.SetActive(false);
        }

        public void UpdateBlobsExcept(EntityBlob _blob)
        {
            foreach(EntityBlob b in blobs) {
                if (_blob != b)
                    b.Next(true);
            }
        }

        public bool CheckVictory(bool isChecked)
        {
            if (isChecked) {
                EntityBlob.State stateTemp = EntityBlob.State.Red;

                for (int i = 0; i < blobs.Count; i++) {
                    if (0 == i) {
                        stateTemp = blobs[i].state;
                        continue;
                    }

                    if (stateTemp != blobs[i].state)
                        return false;
                }

                StartCoroutine(DoVictoryScreen());
        
                return true;
            }

            return false;   
        }

        IEnumerator DoVictoryScreen()
        {
            print(SCRIPTNAME + "You won!");

            foreach (EntityBlob b in blobs) {
                b.gameObject.GetComponent<Renderer>().material.color = Color.black;
                yield return new WaitForSeconds(0.12f);
                b.gameObject.SetActive(false);
            }

            Camera cam = GameObject.FindObjectOfType<Camera>();
            cam.backgroundColor = Color.white;

            textVictory.SetActive(true);

            yield return new WaitForSeconds(2f);
            Application.Quit();
        }


	}
}
