﻿/* Filename: EntityCamera.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist014 {
    [RequireComponent(typeof(Camera))]
	public class EntityCamera : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "EntityCamera.cs: ";

        [SerializeField]
        private float factorScaleThreshold = 4f;

        [SerializeField]
        private float factorCameraSpeed = 4f;

        [SerializeField]
        private float offsetY = 10f;

        private EntityPlayer player;
        private Camera cam;

        private void Start()
        {
            player = FindObjectOfType<EntityPlayer>();

            cam = this.GetComponent<Camera>();
            cam.orthographic = true;
        }

        private void Update()
        {
            // Syncs camera viewport size to player localScale
            cam.orthographicSize = Mathf.Lerp(
                cam.orthographicSize,
                5 + Mathf.Pow(Mathf.Ceil(player.transform.localScale.magnitude / factorScaleThreshold), 3/2),
                factorCameraSpeed * Time.fixedDeltaTime);

            // Syncs camera position with player
            this.transform.position = Vector3.Lerp(
                this.transform.position,
                player.transform.position + offsetY * Vector3.up * Mathf.Ceil(player.transform.localScale.magnitude),
                factorCameraSpeed * Time.fixedDeltaTime);

            Vector3 toPlayer = player.transform.position - this.transform.position;
            this.transform.rotation = Quaternion.Lerp(
                this.transform.rotation,
                Quaternion.LookRotation(toPlayer, Vector3.forward),
                Time.fixedDeltaTime);
        }
	}
}
