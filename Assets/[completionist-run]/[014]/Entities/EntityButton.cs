﻿/* Filename: EntityButton.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace Completionist014 {
	public class EntityButton : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "EntityButton.cs: ";
		
		public void RestartGame(bool willRestart)
        {
            if (willRestart) {
                SceneManager.LoadScene(0);
            }
        }
	}
}
