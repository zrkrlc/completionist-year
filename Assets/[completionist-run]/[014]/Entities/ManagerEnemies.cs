﻿/* Filename: ManagerEnemies.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist014 {
	public class ManagerEnemies : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "ManagerEnemies.cs: ";

        [SerializeField]
        private EntityPlayer player;

        [SerializeField]
        private GameObject prefabBlob;

        [SerializeField]
        private float radiusSpawn = 8f;
        private float radiusSpawnOriginal;

        [SerializeField]
        private int countMaxEnemiesNear = 10;

        [SerializeField]
        private int countMaxEnemiesAll = 100;

        private List<EntityBlob> blobs = new List<EntityBlob>();
        private int countBlobsNearPlayer = 0;

        public void KillBlob (EntityBlob blob)
        {
            blobs.Remove(blob);
            Destroy(blob.gameObject);
        }

        private void Start()
        {
            StartCoroutine(KillFarBlobs());
            radiusSpawnOriginal = radiusSpawn;
        }

        private void Update()
        {
            if ((CheckBlobsCount() < countMaxEnemiesNear) && (CheckBlobsCount() < countMaxEnemiesAll)) {
                print(SCRIPTNAME + "current enemy count at " + blobs.Count + ".");
                SpawnEnemy();
            }

            radiusSpawn = radiusSpawnOriginal * player.transform.localScale.magnitude;
        }
		
		private void SpawnEnemy()
        {
            Transform trf = player.transform;

            GameObject goBlob = (GameObject)Instantiate(
                prefabBlob,
                1.4f * radiusSpawn * trf.localScale.magnitude * new Vector3(UnityEngine.Random.onUnitSphere.x, 0, UnityEngine.Random.onUnitSphere.z),
                Quaternion.identity);
            goBlob.transform.parent = this.transform;

            RegisterBlob(goBlob.GetComponent<EntityBlob>());
        }

        private int CheckBlobsCount()
        {
            countBlobsNearPlayer = 0;
            foreach (Collider hit in Physics.OverlapSphere(player.transform.position, radiusSpawn)) {
                countBlobsNearPlayer += 1;
            }

            return countBlobsNearPlayer;
        }

        private void RegisterBlob(EntityBlob blob)
        {
            // Checks for existence of blob in blobs before adding it
            if (-1 == (int)blobs.IndexOf(blob)) {
                blobs.Add(blob);
            }
        }

        private IEnumerator KillFarBlobs()
        {
            while (true) {
                foreach (EntityBlob blob in blobs) {
                    if ((player.transform.position - blob.transform.position).magnitude > radiusSpawn) {
                        KillBlob(blob);
                    }
                }

                yield return new WaitForSeconds(0.2f);
            }
            yield return null;
        }
	}
}
