﻿/* Filename: EntityPlayer.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist014 {
    [RequireComponent(typeof(Rigidbody))]
	public class EntityPlayer : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "EntityPlayer.cs: ";

        [SerializeField]
        private float factorSpeed = 1.4f;

        [SerializeField]
        private float factorFriction = 2.4f;

        [SerializeField]
        private EntityButton button;

        private enum Direction
        {
            Up,
            Right,
            Down,
            Left
        };

        private Rigidbody rb;
        

        public void ChangeScale(Transform reference)
        {
            this.transform.localScale *= 1 +
                (reference.transform.localScale.magnitude / (reference.transform.localScale.magnitude + this.transform.localScale.magnitude));
        }

        private void OnCollisionEnter(Collision col)
        {
            if (null != col.gameObject.GetComponent<EntityBlob>()) {
                if (col.transform.localScale.magnitude <= this.transform.localScale.magnitude) {
                    ChangeScale(col.transform);
                    col.gameObject.GetComponent<EntityBlob>().Die();
                } else {
                    button.gameObject.SetActive(true);
                    Destroy(this.gameObject);                                  
                }
            }
        }

        private void Start()
        {
            rb = this.GetComponent<Rigidbody>();
            rb.useGravity = false;
            rb.interpolation = RigidbodyInterpolation.Interpolate;
        }
		
		private void Update()
        {
            // Handles input
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
                Move(Direction.Up);
            }
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
                Move(Direction.Left);
            }
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) {
                Move(Direction.Down);
            }
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
                Move(Direction.Right);
            }

            // Simulates friction on player
            rb.velocity = Vector3.Lerp(rb.velocity, Vector3.zero, factorFriction * Time.fixedDeltaTime);

            // Changes player color to match localScale
            this.GetComponent<Renderer>().material.color = new Color(
                0, 
                (32f * this.transform.localScale.magnitude / 100f),
                0);

            factorSpeed = 1.4f + Mathf.Pow(this.transform.localScale.magnitude, 0.5f);
        }

        private void Move(Direction dir)
        {
            rb.velocity = Vector3.zero;

            switch (dir) {
                case Direction.Up:
                    rb.velocity += rb.velocity + factorSpeed * new Vector3(0, 0, 1);
                    break;
                case Direction.Right:
                    rb.velocity += rb.velocity + factorSpeed * new Vector3(1, 0, 0);
                    break;
                case Direction.Down:
                    rb.velocity += rb.velocity + factorSpeed * new Vector3(0, 0, -1);
                    break;
                case Direction.Left:
                    rb.velocity += rb.velocity + factorSpeed * new Vector3(-1, 0, 0);
                    break;
            }
        
        }


    }
}
