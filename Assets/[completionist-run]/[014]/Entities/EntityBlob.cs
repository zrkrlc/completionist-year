﻿/* Filename: EntityBlob.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist014 {
    [RequireComponent(typeof(Rigidbody))]
	public class EntityBlob : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "EntityBlob.cs: ";

        [SerializeField]
        private float factorSpeed = 1.4f;

        [SerializeField]
        private float factorFriction = 2.4f;

        private EntityPlayer player;
        private ManagerEnemies manager;

        private enum Direction
        {
            Up,
            Right,
            Down,
            Left
        };

        private Rigidbody rb;

        public void Die()
        {
            manager.KillBlob(this.GetComponent<EntityBlob>());
        }

        private void Start()
        {
            // Assigns rb
            rb = this.GetComponent<Rigidbody>();
            rb.useGravity = false;
            rb.interpolation = RigidbodyInterpolation.Interpolate;

            // Assigns player
            player = FindObjectOfType<EntityPlayer>();

            // Assigns manager
            manager = FindObjectOfType<ManagerEnemies>();

            // Randomises scale
            Vector3 toPlayer = player.transform.position - this.transform.position;
            this.transform.localScale *= player.transform.localScale.magnitude * UnityEngine.Random.Range(0f, 1.5f);

            StartCoroutine(MoveRandomly());
        }

        private IEnumerator MoveRandomly()
        {
            while (true) {
                Move((Direction)UnityEngine.Random.Range(0, 3));
                yield return new WaitForSeconds(UnityEngine.Random.Range(0f, 0.4f));
            }
            yield return null;
        }

        private void Move(Direction dir)
        {
            rb.velocity = Vector3.zero;

            switch (dir) {
                case Direction.Up:
                    rb.velocity += rb.velocity + factorSpeed * new Vector3(0, 0, 1);
                    break;
                case Direction.Right:
                    rb.velocity += rb.velocity + factorSpeed * new Vector3(1, 0, 0);
                    break;
                case Direction.Down:
                    rb.velocity += rb.velocity + factorSpeed * new Vector3(0, 0, -1);
                    break;
                case Direction.Left:
                    rb.velocity += rb.velocity + factorSpeed * new Vector3(-1, 0, 0);
                    break;
            }

        }
    }
}
