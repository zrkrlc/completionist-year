﻿// File: WorldLattice.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist013
{
	public static class WorldLattice
	{

		private const string SCRIPTNAME = "WorldLattice.cs: ";

        [SerializeField]
        public static float spacing { get { return 1f; } private set { spacing = 1f; } }

        [HideInInspector]
		public enum Direction
        {
            North,
            NorthEast,
            East,
            SouthEast,
            South,
            SouthWest,
            West,
            NorthWest,
        };

        public static Transform ToLattice(this Transform trf)
        {
            trf.position = SnapToLattice(trf.position);
            return trf;
        }

        public static Transform Move(this Transform trf, Direction dir)
        {
            trf = ToLattice(trf);

            Vector3 target = new Vector3();
            switch (dir) {
                case Direction.North:
                    target = new Vector3(0, 0, spacing);
                    break;
                case Direction.NorthEast:
                    target = new Vector3(1, 0, 1).normalized * spacing;
                    break;
                case Direction.East:
                    target = new Vector3(spacing, 0, 0);
                    break;
                case Direction.SouthEast:
                    target = new Vector3(1, 0, -1).normalized * spacing;
                    break;
                case Direction.South:
                    target = new Vector3(0, 0, -spacing);
                    break;
                case Direction.SouthWest:
                    target = new Vector3(-1, 0, -1).normalized * spacing;
                    break;
                case Direction.West:
                    target = new Vector3(-spacing, 0, 0);
                    break;
                case Direction.NorthWest:
                    target = new Vector3(-1, 0, 1).normalized * spacing;
                    break;
            }

            trf.position += target;
            return trf;
        }

        public static Vector3 GetNearestNode(this Vector3 position)
        {
            return SnapToLattice(position);
        }

        private static float SnapToLattice(float f)
        {
            return Mathf.Floor(f / spacing) * spacing;
        }

        private static Vector3 SnapToLattice(Vector3 vec)
        {
            return new Vector3(
                SnapToLattice(vec.x),
                SnapToLattice(vec.y),
                SnapToLattice(vec.z));
        }

        // TODO: implement this
        private static Quaternion SnapToLattice(Quaternion q)
        {
            return q;
        }
	}
}