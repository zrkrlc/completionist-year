﻿// File: AreaSpawn.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist013
{
	public class AreaSpawn : MonoBehaviour
	{

		private const string SCRIPTNAME = "AreaSpawn.cs: ";

        [SerializeField]
        GameObject prefabSpawn;

		void OnMouseDown()
        {
            GameObject go = (GameObject)Instantiate(
                prefabSpawn, 
                new Vector3(
                    Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                    0,
                    Camera.main.ScreenToWorldPoint(Input.mousePosition).z),
                Quaternion.identity);
            //go.transform.ToLattice();
        }
	}
}