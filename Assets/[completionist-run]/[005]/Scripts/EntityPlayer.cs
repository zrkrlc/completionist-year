﻿// File: EntityPlayer.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist005
{
	public class EntityPlayer : MonoBehaviour
	{

		private const string SCRIPTNAME = "EntityPlayer.cs: ";

        [SerializeField]
        Light spotlight;


        float timeElapsed = 0f;
        void FixedUpdate()
        {
            timeElapsed += 0.008f * Time.fixedDeltaTime;
            print(timeElapsed);
            spotlight.color = Color.Lerp(spotlight.color, Color.black, 0.005f * Time.fixedDeltaTime);
        }
	}
}