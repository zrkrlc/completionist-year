﻿// File: MoveTo.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist005
{
    [RequireComponent(typeof(Rigidbody))]
	public class MoveTo : MonoBehaviour
	{

		private const string SCRIPTNAME = "MoveTo.cs: ";

        [SerializeField]
        private Transform target;

        private Rigidbody rb;


        void Start()
        {
            rb = this.GetComponent<Rigidbody>();
        }

		void FixedUpdate()
        {
            Vector3 toTarget = target.transform.position - this.transform.position;
            rb.velocity = Vector3.Lerp(rb.velocity, toTarget, Time.fixedDeltaTime);
        }

        void OnCollisionEnter(Collision col)
        {
            if (null != col.gameObject.GetComponent<EntityPlayer>())
                DestroyImmediate(col.gameObject); 
        }
	}
}