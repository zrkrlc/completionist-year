﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    [SerializeField]
    Transform target;

    [SerializeField]
    float speed = 4f;

    [SerializeField]
    Vector3 offset = new Vector3();
	
	// Update is called once per frame
	void Update () {
        this.transform.position = Vector3.Lerp(
            this.transform.position,
            target.transform.position + offset,
            speed * Time.deltaTime);

        Vector3 toTarget = target.transform.position - this.transform.position;
        this.transform.rotation = Quaternion.Slerp(
            this.transform.rotation,
            Quaternion.LookRotation(toTarget, Vector3.up),
            speed * Time.deltaTime);
	}
}
