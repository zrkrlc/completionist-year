﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Controls : MonoBehaviour {

    [SerializeField]
    float speedForward = 6f;

    [SerializeField]
    float speed = 4f;

    [SerializeField]
    float depthDeath = -10f;

    private Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = this.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, speedForward);

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) {
            rb.velocity = new Vector3(speed * -Vector3.right.x, rb.velocity.y, speedForward);
        }
        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow)) {
            rb.velocity = new Vector3(0f, rb.velocity.y, speedForward);
        }

        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) {
            rb.velocity = new Vector3(speed * Vector3.right.x, rb.velocity.y, speedForward);
        }
        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow)) {
            rb.velocity = new Vector3(0f, rb.velocity.y, speedForward);
        }


        // Kills player upon falling
        if (this.transform.position.y <= depthDeath) {
            SceneManager.LoadScene(0);
        }
    }
}
