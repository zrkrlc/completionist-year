﻿// File: ManagerTriggers.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist010
{
	public class ManagerTriggers : MonoBehaviour
	{

		private const string SCRIPTNAME = "ManagerTriggers.cs: ";

        private List<EntityTrigger> triggers = new List<EntityTrigger>();
        private List<EntityTarget> targets = new List<EntityTarget>();

		void Start () {
            foreach (EntityTarget target in FindObjectsOfType<EntityTarget>()) {
                targets.Add(target);
            }
			foreach (EntityTrigger trigger in FindObjectsOfType<EntityTrigger>()) {
                triggers.Add(trigger);
                trigger.target = targets[Random.Range(0, targets.Count - 1)].transform;             
            }

		}
	
	}
}