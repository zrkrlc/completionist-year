﻿// File: EntityPlayer.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist010
{
	public class EntityPlayer : MonoBehaviour
	{

		private const string SCRIPTNAME = "EntityPlayer.cs: ";

        [SerializeField]
        private float factorSpeed = 2.0f;

        private Rigidbody rb;

        void Start()
        {
            rb = this.GetComponent<Rigidbody>();
        }

        void Update()
        {
            // Handles input
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
                rb.AddForce(factorSpeed * -Vector3.right, ForceMode.Impulse);
            }
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
                rb.AddForce(factorSpeed * Vector3.right, ForceMode.Impulse);
            }
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) {
                rb.AddForce(factorSpeed * -Vector3.forward, ForceMode.Impulse);
            }
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
                rb.AddForce(factorSpeed * Vector3.forward, ForceMode.Impulse);
            }
        }
	}
}