﻿// File: EntityTrigger.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist010
{
	public class EntityTrigger : MonoBehaviour
	{

		private const string SCRIPTNAME = "EntityTrigger.cs: ";
        
        public enum State
        {
            Active,
            Inactive
        }

        public Transform target;
       
        private float delayTeleport = 0.15f;
        private float durationStay = 0f;

        void OnCollisionEnter(Collision col)
        {
            durationStay = 0f;
            StartCoroutine(Teleport(col.transform, target));

            // Changes colour
            if (null != col.gameObject.GetComponent<EntityPlayer>()) {
                EntityWalls walls = target.transform.parent.GetComponentInChildren<EntityWalls>();
                this.GetComponent<Renderer>().material.color = walls.color;
            }
        }

        private IEnumerator Teleport(Transform source, Transform dest)
        {
            while (durationStay <= delayTeleport) {
                durationStay += Time.deltaTime;
                yield return null;
            }

            if (null != source.gameObject.GetComponent<EntityPlayer>()) {
                // Cancels all movement
                Rigidbody rb = source.GetComponent<Rigidbody>();
                if (null != rb) {
                    rb.velocity = Vector3.zero;
                    rb.angularVelocity = Vector3.zero;
                }

                source.transform.position = this.transform.position;
                yield return null;

                source.transform.position = dest.transform.position;
            }

            yield return null;
        }
	}
}