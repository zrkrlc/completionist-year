﻿// File: EntityWalls.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist010
{
	public class EntityWalls : MonoBehaviour
	{

		private const string SCRIPTNAME = "EntityWalls.cs: ";

        [HideInInspector]
        public Color color = new Color();

		void Start () {
            color = new Color(
                    Random.Range(0f, 1f),
                    Random.Range(0f, 1f),
                    Random.Range(0f, 1f),
                    Random.Range(0f, 1f));

            foreach (Transform trf in this.transform) {
                trf.gameObject.GetComponent<Renderer>().material.color = color;
            }
		}
		
		
	}
}