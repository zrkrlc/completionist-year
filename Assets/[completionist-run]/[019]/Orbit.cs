﻿// File: Orbit.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist019
{
	public class Orbit : MonoBehaviour
	{
		private const string SCRIPTNAME = "Orbit.cs: ";

        [SerializeField]
        private float maxDistanceToInfluence = 1000f;

        void Update()
        {
            foreach (Collider col in Physics.OverlapSphere(this.transform.position, maxDistanceToInfluence)) {
                if (col.gameObject != this.gameObject) {
                    Vector3 toCenter = this.transform.position - col.transform.position;
                    col.GetComponent<Rigidbody>().AddForce(
                        (this.GetComponent<Rigidbody>().mass * col.GetComponent<Rigidbody>().mass * toCenter) 
                        / (toCenter.magnitude * toCenter.magnitude));
                }
            }

            if (this.transform.position.magnitude >= 1000f) {
                Destroy(this.gameObject);
            }
        }
	}
}