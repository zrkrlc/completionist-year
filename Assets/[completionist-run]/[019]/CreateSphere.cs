﻿// File: CreateSphere.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Veer.House
{
	public class CreateSphere : MonoBehaviour
	{
		private const string SCRIPTNAME = "CreateSphere.cs: ";

        [SerializeField]
        private GameObject prefabSphere;

        [SerializeField]
        private float maxInitialVelocity = 20f;

        [SerializeField]
        private float maxInitialMass = 10f;

        private Camera cam;

        void Start()
        {
            cam = Camera.main;
        }

		void Update()
        {
            if (Input.GetMouseButtonDown(0)) {
                Vector3 newPos = Vector3.zero;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Plane hPlane = new Plane(Vector3.up, Vector3.zero);
                float distance = 0;
                if (hPlane.Raycast(ray, out distance)) {
                    newPos = ray.GetPoint(distance);
                    newPos.x *= this.transform.position.z;
                    newPos.y *= this.transform.position.z;
                }

                Rigidbody rb = ((GameObject) Instantiate(prefabSphere, newPos, Quaternion.identity, this.transform)).GetComponent<Rigidbody>();
                rb.velocity = new Vector3(
                    UnityEngine.Random.Range(-maxInitialVelocity, maxInitialVelocity),
                    UnityEngine.Random.Range(-maxInitialVelocity, maxInitialVelocity),
                    0);
                rb.mass = Mathf.Pow(UnityEngine.Random.Range(1f, maxInitialMass), 2);
                rb.transform.localScale = Vector3.one * Mathf.Sqrt(rb.mass);
            }
        }
	}
}