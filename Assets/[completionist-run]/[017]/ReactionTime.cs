﻿/* Filename: ReactionTime.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


namespace Interlude {
    [RequireComponent(typeof(Camera))]
	public class ReactionTime : MonoBehaviour
	{
	
		private const string SCRIPTNAME = "ReactionTime.cs: ";

        [SerializeField]
        Text textStart;

        [SerializeField]
        Text textWin;

        [SerializeField]
        Text textLose;

        private float timeAtClick = 0f;
        private float timeAtTurnRed = 0f;

        private bool hasStarted = false;
        private bool hasTurnedRed = false;

        private Camera cam;

        void Start()
        {
            // Shows startgame text
            textStart.gameObject.SetActive(true);

            // Hides endgame texts
            textWin.gameObject.SetActive(false);
            textLose.gameObject.SetActive(false);

            // Assigns camera
            cam = Camera.main;

            // Starts game
            StartCoroutine(StartRound());
        }

        void Update()
        {           
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) {
                timeAtClick = Time.timeSinceLevelLoad;
                textStart.gameObject.SetActive(false);
                StopAllCoroutines();
                StartCoroutine(EndRound(hasTurnedRed));
            }
        }


        private IEnumerator StartRound() {
            print(SCRIPTNAME + "Starting game...");            
            StartCoroutine(PlayRound(UnityEngine.Random.Range(2.6f, 12f)));
            yield return null;
        }
		
		private IEnumerator PlayRound(float durationEnd, float durationColorStay = 4f)
        {
            yield return new WaitForSeconds(2.6f);
            textStart.gameObject.SetActive(false);

            float duration = 0f;
            bool hasChangedColour = false;
            while (duration <= durationEnd) {
                duration += Time.fixedDeltaTime;

                if (0 == (Mathf.Floor(duration % durationColorStay)) && !hasChangedColour) {
                    hasChangedColour = true;
                                            
                    cam.backgroundColor = new Color(
                    UnityEngine.Random.Range(0f, 0.4f),
                    UnityEngine.Random.Range(0.4f, 1f),
                    UnityEngine.Random.Range(0.4f, 1f),
                    1f);

                    print(SCRIPTNAME + "Changing color to " + cam.backgroundColor.ToString() + "...");
                } else {
                    hasChangedColour = false;
                }

                yield return new WaitForFixedUpdate();                             
            }

            hasTurnedRed = true;
            timeAtTurnRed = durationEnd + 2.6f;
            cam.backgroundColor = Color.red;
        }

        private IEnumerator EndRound(bool hasWon)
        {
            print(SCRIPTNAME + "Ending game...");
            if (hasWon) {
                textWin.gameObject.SetActive(true);
                cam.backgroundColor = Color.blue;
                textWin.text = "YOU WERE TOO SLOW BY " + (timeAtClick - timeAtTurnRed).ToString("F3") + "s";
            } else {
                textLose.gameObject.SetActive(true);
                cam.backgroundColor = Color.yellow;
                textLose.text = "YOU WERE TOO FAST BY " + (timeAtTurnRed - timeAtClick).ToString("F3") + "s";
            }
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene(0);
            yield return null;
        }
	}
}
