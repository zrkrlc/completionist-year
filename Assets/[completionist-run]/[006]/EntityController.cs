﻿// File: EntityController.cs
// Description: 
// How to use:
// 	0) Attach to controller.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist006
{
    [RequireComponent(typeof(SteamVR_TrackedObject))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Collider))]
    public class EntityController : MonoBehaviour
	{

		private const string SCRIPTNAME = "EntityController.cs: ";

        // Fields for SteamVR
        private SteamVR_TrackedObject tracked;
        private SteamVR_Controller.Device device;


        // Fields for grabbing
        private Rigidbody rb;
        private Collider colAttachPoint;
        private GameObject go;
        private bool isInTrigger = false;

        [SerializeField]
        private Collider colModel;
        private FixedJoint joint = null;

        void Start()
        {
            // Assigns devices to correct controllers
            device = SteamVR_Controller.Input((int)this.GetComponent<SteamVR_TrackedObject>().index);

            // Assigns tracked object
            tracked = this.GetComponent<SteamVR_TrackedObject>();

            // Assigns Rigidbody
            rb = this.GetComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.useGravity = false;

            // Assigns trigger
            colAttachPoint = this.GetComponent<Collider>();
            colAttachPoint.isTrigger = true;
        }

        // LateUpdate to run after OnTrigger events finish
        void LateUpdate()
        {
            // TODO: abstract grabbing to a higher-order function that takes OnGrabDown, OnGrabStay, OnGrabRelease delegates
            /// How grabbing is implemented
            /// 1) Upon trigger down, get an object in the trigger area
            /// 2) Disable controller model collider (to avoid collider overlap)
            /// 3) Attach said object to joint
            /// 4) Upon trigger up, release object with appropriate velocities
            if (joint == null && device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger)) {
                print(SCRIPTNAME + "grabbing " + go.name + "...");
                // Disables model collider
                colModel.enabled = false;

                go.transform.position = rb.transform.position;
                joint = go.AddComponent<FixedJoint>();
                joint.connectedBody = rb;
            }
            else if (joint != null && device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger)) {
                colModel.enabled = true;

                if (isInTrigger) {
                    GameObject goJoint = joint.gameObject;
                    Rigidbody rbGoJoint = goJoint.GetComponent<Rigidbody>();

                    // <boilerplate>
                    Object.DestroyImmediate(joint);
                    joint = null;

                    var origin = (null != tracked.origin) ? tracked.origin : tracked.transform.parent;
                    if (origin != null) {
                        // Adjusts frisbee velocity; hand-tweaked
                        rbGoJoint.velocity = origin.TransformVector(device.velocity * Mathf.Sqrt(device.velocity.magnitude) * 1.8f);
                        rbGoJoint.angularVelocity = origin.TransformVector(device.angularVelocity);
                    } else {
                        rbGoJoint.velocity = device.velocity;
                        rbGoJoint.angularVelocity = device.angularVelocity;
                    }

                    rbGoJoint.maxAngularVelocity = rbGoJoint.angularVelocity.magnitude;
                    // </boilerplate>
                }

                // Clears joint once object leaves trigger area
                else {
                    joint = null;
                }
            }
        }

        void OnTriggerEnter(Collider hit)
        {
            print(SCRIPTNAME + hit.name + " ready for grabbing.");
            isInTrigger = true;

            go = hit.gameObject;
        }

        void OnTriggerExit(Collider hit)
        {
            go = null;
        }


    }
}