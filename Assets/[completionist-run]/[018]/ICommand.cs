﻿// File: ICommand.cs
// Description: 

using System.Collections;
using System.Collections.Generic;

namespace Completionist018
{
	public interface ICommand<T>
	{
        List<T> inputs { get; set; }
        void Apply(params T[] args);
        void Unapply(params T[] args);
	}
}