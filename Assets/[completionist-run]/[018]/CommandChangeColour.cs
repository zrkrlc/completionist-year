﻿// File: CommandChangeColour.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist018
{
	public class CommandChangeColour : MonoBehaviour, ICommand<GameObject>
	{
		private const string SCRIPTNAME = "CommandChangeColour.cs: ";

        [SerializeField]
        private Color color = Color.black;

        #region ICommand<GameObject>
        public List<GameObject> inputs { get; set; }
        public void Apply(params GameObject[] args)
        {
            for (int i = 0; i < args.Length; i++) {
                inputs[i] = args[i];
            }

            for (int i = 0; i < args.Length; i++) {
                args[i].GetComponent<Renderer>().material.color = color;
            }
        }

        public void Unapply(params GameObject[] args)
        {
            GameObject[] temp = args;

            for (int i = 0; i < args.Length; i++) {
                try {
                    args[i].GetComponent<Renderer>().material.color = inputs[i].GetComponent<Renderer>().material.color;
                } catch (System.IndexOutOfRangeException e) {
                    Debug.LogError(SCRIPTNAME + "No previous color application.");
                } catch (System.NullReferenceException e) {
                    Debug.LogError(SCRIPTNAME + "No objects with materials found.");
                }
            }
        }
        #endregion

        void Awake()
        {
            inputs = new List<GameObject>();
        }
    }
}