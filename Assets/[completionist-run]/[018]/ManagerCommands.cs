﻿// File: ManagerCommands.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist018
{
	public class ManagerCommands : MonoBehaviour
	{
		private const string SCRIPTNAME = "ManagerCommands.cs: ";

        [SerializeField]
        private GameObject[] tools;

        private List<GameObject> selection = new List<GameObject>();
        private List<ICommand<GameObject>> queueCommand = new List<ICommand<GameObject>>();
        private int queueIndex = 0;

        
        private void Update()
        {
            queueIndex = (queueIndex <= 0 ? 0 : (queueIndex >= selection.Count ? selection.Count - 1 : queueIndex));

            // Selects all GameObjects in scene
            if (Input.GetKeyDown(KeyCode.F)) {
                foreach (GameObject go in GameObject.FindObjectsOfType<GameObject>()) {
                    if (!selection.Contains(go)) {
                        print(SCRIPTNAME + go.name + " selected.");
                        selection.Add(go);
                    }
                }
            }


            // Applies tools to selected GameObjects
            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                var command = ((GameObject) Instantiate(tools[0], this.transform)).GetComponent<ICommand<GameObject>>();
               
                command.Apply(selection.ToArray());

                queueCommand.Add(command);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2)) {
                var command = ((GameObject) Instantiate(tools[1], this.transform)).GetComponent<ICommand<GameObject>>();
                command.Apply(selection.ToArray());

                queueCommand.Add(command);
            }


            // Undos commands
            if (Input.GetKeyDown(KeyCode.Z)) {
                var command = queueCommand[queueIndex--];
                command.Unapply(command.inputs.ToArray());
            }

            // Redos commands
            if (Input.GetKeyDown(KeyCode.Y)) {
                var command = queueCommand[queueIndex++];
                command.Apply(command.inputs.ToArray());
            }
        
        }
	}
}