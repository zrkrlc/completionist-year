﻿// File: CommandCreate.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Completionist018
{
	public class CommandCreate : MonoBehaviour, ICommand<GameObject>
	{
		private const string SCRIPTNAME = "CommandCreate.cs: ";

        #region ICommand<GameObject>
        public List<GameObject> inputs { get; set; }
        public void Apply(params GameObject[] args)
        {
            for (int i = 0; i < args.Length; i++) {
                inputs[i] = args[i];
            }

            for (int i = 0; i < args.Length; i++) {
                Instantiate(args[i], this.transform, false);
            }
        }

        public void Unapply(params GameObject[] args)
        {
            for (int i = 0; i < args.Length; i++) {
                inputs[i] = args[i];
            }

            for (int i = 0; i < args.Length; i++) {
                Destroy(args[i]);
            }
        }
        #endregion

        void Awake()
        {
            inputs = new List<GameObject>();
        }
    }
}