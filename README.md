# Introduction

Thirteen years ago, Raph Koster laid out a theory of games [^1]. For him, all games are essentially edutainment, teaching us primitive survival skills like:

* establishing dominance
* navigating spaces
* resource management
* aiming

All game design then is an arms race with human brains and their ever-increasing capacity for pattern recognition. But there is a gap that needs to be crossed. There is a gap that separates mere pattern-treadmills from art that lends itself to interpretation. This project is an attempt to build bridges. 

For 30 days, I shall:

1) Build and complete a game interaction or mechanic.

2) Commit it to this repository.

3) Write a post about it on my [blog](https://zrkrlc.wordpress.com).

No, I am not promising to build Golden Gates every day -- I am simply too small and too few. But I will try to finish small bits at a time, like [this guy](https://en.wikipedia.org/wiki/Justo_Gallego_Mart%C3%ADnez), which I hope will add up to something worth looking back on.

----


[^1]: His 2003 Austin Games Conference [presentation](http://www.theoryoffun.com/theoryoffun.pdf)

