﻿// File: HelperBillboard.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Toolbox
{
	public class HelperBillboard : MonoBehaviour
	{

		private const string SCRIPTNAME = "HelperBillboard.cs: ";

        [SerializeField]
        public Transform target;

        [SerializeField]
        private float factorRotationSpeed = 6.0f;

        void Start()
        {
            // By default, faces player
            if (null == target)
                target = Camera.main.transform;
        }
		
		void Update () {
            Vector3 toTarget = target.position - this.transform.position;
            this.transform.rotation = Quaternion.Slerp(
                this.transform.rotation,
                Quaternion.LookRotation(toTarget),
                factorRotationSpeed * Time.fixedDeltaTime);
			
		}
	}
}