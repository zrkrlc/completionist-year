﻿// File: SnapToLattice.cs
// Description: 
// How to use:
// 	0) Attach to object you want to snap to lattice.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Veer.House.Prototyping
{
	public class SnapToLattice : MonoBehaviour
	{

		private const string SCRIPTNAME = "SnapToLattice.cs: ";

        public bool isSnappedLinear = false;
        public bool isSnappedAngular = false;

        [SerializeField]
        private float spacingLinear = 1f;

        [SerializeField]
        private float spacingAngular = 15f;

        // TODO: remove conditionals
		private void LateUpdate()
        {
            // TODO: remove magic
            // MAGIC 
            spacingLinear *= 0.2f;
                   
            if (isSnappedLinear) {
                this.transform.position = SnapLinear(this.transform.position);
            }

            if (isSnappedAngular) {
                this.transform.rotation = SnapAngular(this.transform.rotation);
            }

            spacingLinear *= 5f;
        }

        // TODO: check for division by zero error
        private float SnapLinear(float f)
        {
            return Mathf.Floor(f / spacingLinear) * spacingLinear;
        }
       
        private Vector3 SnapLinear(Vector3 v)
        {
            return new Vector3(SnapLinear(v.x), SnapLinear(v.y), SnapLinear(v.z));
        }

        private float SnapAngular(float f)
        {
            return (Mathf.Floor(f / spacingAngular) * spacingAngular) % 360f;
        }

        // TODO: prevent gimbal lock
        private Quaternion SnapAngular(Quaternion q)
        {
            return Quaternion.Euler(
                SnapAngular(q.eulerAngles.x), 
                SnapAngular(q.eulerAngles.y), 
                SnapAngular(q.eulerAngles.z));
        }
        
	}
}