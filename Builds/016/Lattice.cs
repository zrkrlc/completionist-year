﻿/* Filename: Lattice.cs
/* Description: 
/* How to use: 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Completionist016 {
	public class Lattice : MonoBehaviour
	{	
		private const string SCRIPTNAME = "Lattice.cs: ";

        public enum Direction
        {
            North,
            NorthEast,
            East,
            SouthEast,
            South,
            SouthWest,
            West,
            NorthWest
        };

        public List<Node> lattice = new List<Node>();

        [SerializeField]
        private int countRows;

        [SerializeField]
        private int countCols;

        [SerializeField]
        private bool willGenerateGameObjects = false;

        [SerializeField]
        private bool hasRandomisedWeights = false;

        #region MonoBehaviours
        void Awake()
        {
            lattice = GenerateLatticeRectangular(countRows, countCols, willGenerateGameObjects, hasRandomisedWeights);
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.black;
            foreach (Node n in lattice) {
                Gizmos.DrawSphere(n.position, 0.1f);
            }
        }
        #endregion

        #region Methods
        public List<Node> GenerateLatticeRectangular(
            int countRows, 
            int countCols, 
            bool willGenerateGameObjects = false, 
            bool hasRandomisedWeights = false)
        {
            List<Node> output = new List<Node>();

            for (int row = 0; row < countRows; row++) {
                for (int col = 0; col < countCols; col++) {
                    Node n = new Node();
                    output.Add(n);

                    // Sets other parameters
                    n.position = this.transform.position + new Vector3(col, -row, 0f);
                    n.weight = hasRandomisedWeights ? UnityEngine.Random.Range(1f, float.MaxValue) : 1f;

                    // Optionally generates empty GameObjects on nodes
                    if (willGenerateGameObjects) {
                        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        go.name = "Node";
                        go.transform.position = n.position;
                        go.transform.parent = this.transform;
                    }            
                }
            }

            for (int row = 0; row < countRows; row++) {
                for (int col = 0; col < countCols; col++) {
                    Node n = output[(row * countRows) + col];

                    // Sets neighbours
                    // NOTE: if neighbour does not exist, the node itself takes its place
                    n.neighbours.North = (0 == row) ? n : output[(row - 1) * countCols + col];
                    n.neighbours.NorthEast = (0 == row || countCols - 1 == col) ? n : output[(row - 1) * countCols + col + 1];
                    n.neighbours.East = (countCols - 1 == col) ? n : output[(row * countCols) + col + 1];
                    n.neighbours.SouthEast = (countRows - 1 == row || countCols - 1 == col) ? n : output[(row + 1) * countCols + col + 1];
                    n.neighbours.South = (countRows - 1 == row) ? n : output[(row + 1) * countCols + col];
                    n.neighbours.SouthWest = (countRows - 1 == row || 0 == col) ? n : output[(row + 1) * countCols + col - 1];
                    n.neighbours.West = (0 == col) ? n : output[(row * countCols) + col - 1];
                    n.neighbours.NorthWest = (0 == row || 0 == col) ? n : output[(row - 1) * countCols + col - 1];
                }
            }

            return output;
        }

        public static bool CheckLattice(List<Node> lattice)
        {
            foreach (Node n in lattice) {
                if (null == n) {
                    return false;
                }
            }

            return true;
        }

        // Gets neighbours of node n, clockwise from North
        public static List<Node> GetNeighbours(Node n)
        {
            List<Node> output = new List<Node>();

            output.Add(n.neighbours.North);
            output.Add(n.neighbours.NorthEast);
            output.Add(n.neighbours.East);
            output.Add(n.neighbours.SouthEast);
            output.Add(n.neighbours.South);
            output.Add(n.neighbours.SouthWest);
            output.Add(n.neighbours.West);
            output.Add(n.neighbours.NorthWest);

            return output;
        }

        // TODO: give path as List<Direction>
        public static List<Node> CalculateShortestPath(Node source, Node destination, int maxIterations = 10000, int maxLookupDepth = 100)
        {
            List<Node> output = new List<Node>();

            Node nodeCurrent = source;
            List<Node> nodesChecked = new List<Node>();
            int iterations = 0;           
            while (!ReferenceEquals(nodeCurrent, destination) && iterations <= maxIterations) {                
                int lookupDepth = 0;


                while (lookupDepth <= maxLookupDepth) {
                    iterations += 1;
                    lookupDepth += 1;

                    // Obtains neighbours and adds minimum node to path
                    List<Node> neighboursCurrent = new List<Node>();
                    foreach (Node n in GetNeighbours(nodeCurrent)) {
                        neighboursCurrent.Add(n);
                    }                  
                    
                    foreach (Node n in nodesChecked) {
                        neighboursCurrent.Remove(n);
                    }
                                       
                    float currentMinWeight = CalculateWeightFunction(nodeCurrent, destination);
                    int indexMinWeight = 0;
                    for (int i = 0; i < neighboursCurrent.Count; i++) {
                        if (CalculateWeightFunction(neighboursCurrent[i], destination) < currentMinWeight) {
                            indexMinWeight = i;
                            currentMinWeight = CalculateWeightFunction(neighboursCurrent[i], destination);
                        }
                        nodesChecked.Add(neighboursCurrent[i]);
                    }
                    nodeCurrent = neighboursCurrent[indexMinWeight];
                    output.Add(neighboursCurrent[indexMinWeight]);

                    // TODO: prune checked nodes from search
                    // TODO: backtrack
                }                
            }

            return output;
        }
        #endregion

        #region Helpers
        private static float CalculateWeightFunction(Node nodeTest, Node destination)
        {            
            Vector3 toDest = destination.position - nodeTest.position;
            return nodeTest.weight * toDest.sqrMagnitude;
        }
        #endregion


    }


    public class Node
    {
        public struct Neighbours
        {
            public Node North;
            public Node NorthEast;
            public Node East;
            public Node SouthEast;
            public Node South;
            public Node SouthWest;
            public Node West;
            public Node NorthWest;
        }

        public Neighbours neighbours;
        public Vector3 position;
        public float weight;
    }
}
